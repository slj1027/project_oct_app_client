import request from '../utils/http';

export const getCart = (data) =>
	request.post(`/p/shopCart/info`, {
		method: `post`,
		data,
	});
//添加购物车
export const addCart = (data) =>
	request.post(`/p/shopCart/changeItem`, {
		method: `post`,
		data,
	});
//获取购物车商品数量
export const getCartLength = (data) =>
	request.get(`/p/shopCart/prodCount`, {
		method: `get`,
		data,
	});
//获取选中购物项总计、选中的商品数量
export const getCartSum = (data) =>
	request.get(`/p/shopCart/totalPay`, {
		method: `get`,
		data,
	});
//删除

export const delCart = (data) =>
	request.delete(`/p/shopCart/deleteItem`, {
		method: `delete`,
		data,
	});
// 获取失效商品
export const getLapseLength = () =>
	request.get(`/p/shopCart/expiryProdList`, {
		method: `get`,
	});
