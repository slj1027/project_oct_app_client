import request from '../utils/http';

export const login = (data) =>
  request.post(`/login?grant_type=mini_app`, { method: `post`, data });
export const getClassData = () =>
  request.get(`/category/categoryInfo`, { method: `get` });
export const getClassDataItem = (data) =>
  request.get(`/prod/pageProd`, {
    method: `get`,
    data,
  });

//首页
//获取轮播图数据
export const getIndexImg = () => request.get(`/indexImgs`, { method: `get` });
//获取宫格新品推荐数据
export const getnewShop = (query) =>
   request.get(`/prod/lastedProdPage`,{
      method:"get",
      data:{
         current:query.current,
         size:query.size
      }
   })
//获取宫格限时特惠数据
export const getLimited = () =>
  request.get(`/prod/discountProdList`, { method: 'get' });
//获取宫格限时特惠数据
export const getDaily = () =>
  request.get(`/prod/moreBuyProdList`, { method: `get` });
//获取公告数据
export const getNotice = () =>
  request.get('/shop/notice/topNoticeList', { method: `get` });
//获取公告数据详情
export const getNoticeDetail = (id) => {
  return request.get(`/shop/notice/info/${id}`, { method: `get` });
};

//获取每日上新数据
export const getNewDay = () =>
  request.get(`/prod/prodListByTagId?tagId=1&size=6`, { method: `get` });
//获取商城热卖数据
export const shopHot = () =>
  request.get(`/prod/lastedProdPage`, { method: `get` });
// export const shopHot = () =>
//    request.get(`/prod/prodListByTagId?tagId=1&size=6`, { method: `get` })
//获取更多宝贝
export const moreBaby = () =>
  request.get(`/prod/lastedProdPage`, { method: `get` });
// export const moreBaby = () =>
//    request.get(`/prod/prodListByTagId?tagId=1&size=6`, { method: `get` })
//获取搜索结果
export const getSearch = (query) => {
  return request.get(`/search/searchProdPage`, {
    method: `get`,
    data: {
      current: query.current,
      prodName: query.prodName,
      size: query.size,
      sort: query.sort,
    },
  });
};
