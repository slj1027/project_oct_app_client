import request from '../utils/http';
//添加/取消收藏
export const addOrCancel = (data) =>
  request.post(`/p/user/collection/addOrCancel`, {
    method: `post`,
    data,
  });
//判断是否在收藏列表中
export const isInOrCancel = (id) =>
  request.get(`/p/user/collection/isCollection`, {
    method: `get`,
    data: {
      prodId: id,
    },
  });
//获取用户收藏列表
export const getCollection = (data) =>
  request.get(`/p/user/collection/prods`, {
    method: `get`,
    data,
  });
export const getCollectionLength = () =>
  request.get(`/p/user/collection/count`, {
    method: `get`,
  });
