import request from '../utils/http';
//结算，生成订单信息
export const commitOrder = (data) =>
  request.post(`/p/order/confirm`, {
    method: `post`,
    data,
  });
//提交订单，返回支付流水号
export const getOrderNumber = (data) =>
  request.post(`/p/order/submit`, {
    method: `post`,
    data,
  });

//获取订单列表
export const getAllOrderList = (data) =>
  request.get(`/p/myOrder/myOrder`, {
    method: `get`,
    data,
  });
export const getAllOrderDetail = (data) =>
  request.get(`/p/myOrder/orderDetail`, {
    method: `get`,
    data,
  });

//根据订单号进行支付
export const putOrder = (data) =>
  request.post(`/p/order/pay`, {
    method: `post`,
    data,
  });
//取消订单
export const cancelOrder = (data) =>
  request.put(`/p/myOrder/cancel/${data}`, {
    method: `put`,
  });
//获取订单数量
export const getOrderLength = () =>
  request.get(`/p/myOrder/orderCount`, {
    method: `get`,
  });
export const delOrder = (orderNumber) =>
  request.delete(`/p/myOrder/${orderNumber}`, {
    method: `delete`,
  });
export const hadOrder = (orderNumber) =>
  request.put(`/p/myOrder/receipt/${orderNumber}`, {
    method: `put`,
  });
export const comment = (data) =>
  request.post(`/prodComm`, {
    method: `post`,
    data,
  });
