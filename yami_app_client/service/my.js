import http from '../utils/http';

export const getCode = (data) =>
  http.post('/p/sms/send', {
    method: `post`,
    data
  });
