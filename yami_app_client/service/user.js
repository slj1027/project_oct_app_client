import http from '../utils/http';

// 获取地址列表
export const getAddr = (data) =>
  http.get('/p/area/listByPid', {
    method: `get`,
    data,
  });
// 获取地址列表
export const addAddrList = () =>
  http.get('/p/address/list', {
    method: `get`,
  });
// 保存地址
export const addAddrApi = (params) =>
  http.post('/p/address/addAddr', {
    method: 'post',
    data: params,
  });
//编辑地址回显
export const addAddrEdita = (id) =>
  http.get(`/p/address/addrInfo/${id}`, {
    method: 'get',
  });
//编辑地址
export const addAddrUpdate = (data) =>
  http.put(`/p/address/updateAddr`, {
    method: 'put',
    data: data,
  });
//默认地址
export const defaultAddr = (id) =>
  http.put(`/p/address/defaultAddr/${id}`, {
    method: 'put',
  });

//删除地址
export const addAddrDel = (id) =>
  http.delete(`/p/address/deleteAddr/${id}`, {
    method: 'delete',
  });
