import request from '../utils/http';

export const getDetail = (id) =>
  request.get(`/prod/prodInfo`, {
    method: `get`,
    data: {
      prodId: id,
    },
  });
export const getDetailComment = (id) =>
  request.get(`/prodComm/prodCommData`, {
    method: `get`,
    data: {
      prodId: id,
    },
  });
export const getSku = (id) =>
  request.get(`/sku/getSkuList`, {
    method: `get`,
    data: {
      prodId: id,
    },
  });
