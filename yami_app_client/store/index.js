// 页面路径：store/index.js
import Vue from 'vue';
import Vuex from 'vuex';
import {
	getCartLength
} from '@/service/cart.js'
Vue.use(Vuex); //vue的插件机制

//Vuex.Store 构造器选项
const store = new Vuex.Store({
	state: {
		//存放状态
		cartLength: 0,
		addrId: 0,
		orderBasketIds: '',
		user: uni.getStorageSync('userInfo') ? uni.getStorageSync('userInfo') : {},
	},
	mutations: {
		getUserInfo(state, Info) {
			state.user = Info;
		},
		getAddrId(state, Info) {
			state.addrId = Info;
		},
		getOrderBasketIds(state, Info) {
			state.orderBasketIds = Info;
		},
		getCartLength(state, Info) {
			state.cartLength = Info
			uni.setTabBarBadge({
				index: 3,
				text: Info.toString(),
			})
		}
	},
	actions: {
		async getCartLength(context, payload) {
			let res = await getCartLength()
			context.commit('getCartLength', res)
		},
	},
});
export default store;
