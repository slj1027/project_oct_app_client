import App from './App';

// #ifndef VUE3
import Vue from 'vue'
import store from './store'
import BaseImage from "@/components/errorImage"

Vue.component('base-image',BaseImage);

Vue.prototype.$store = store;
//云函数
async function newClond() {
  const n_cloud = new wx.cloud.Cloud({
    resourceEnv: 'min-gucof',
    traceUser: true,
  });
  //初始化
  await n_cloud.init();
  return n_cloud;
}

Vue.prototype.$cloud = newClond();
Vue.config.productionTip = false;
App.mpType = 'app';
const app = new Vue({
  store,
  ...App,
});
app.$mount();
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue';
export function createApp() {
  const app = createSSRApp(App);
  return {
    app,
  };
}
// #endif