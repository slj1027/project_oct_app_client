export const formatUrl = (url, data) => {
  return data && Object.keys(data).length > 0
    ? url +
        '?' +
        Object.keys(data)
          .map((item) => (item = data[item]))
          .join('&')
    : url;
};
