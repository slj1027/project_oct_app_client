import result from './config';
import { formatUrl } from './formatUrl';
import Vue from 'vue';
const { baseUrl, whiteList } = result;

export const http = ({timeout, baseUrl, errorHandle, requestHandle}) => {
  const request = async (
    url,
    option = { method: 'get', data: {}, header: {} }
  ) => {
    let { method, data, header } = option;
    const requestOption = {
      originUrl: url,
      baseUrl,
      url: `${baseUrl}${url}`,
      method,
      data,
      header,
      timeout,
    };
    const config =
      (requestHandle && requestHandle(requestOption)) || requestOption;
    if (typeof config === 'object' && config.url && config.method) {
      try {
        const res = await uni.request(config);

        if (Array.isArray(res) && res.length > 0) {
          return res[1] && res[1].data ? res[1].data : res[1];
        }
        return Promise.reject(res);
      } catch (error) {
        return Promise.reject(error);
      }
    }
  };
  return {
    get(url, options) {
      const hasFormatUrl = formatUrl(url, options.data);
      return request(hasFormatUrl, options);
    },
    post(url, options) {
      return request(url, options);
    },
    put(url, options) {
      return request(url, options);
    },
    delete(url, options) {
      return request(url, options);
    },
  };
};

const request = http({
  timeout: 30000,
  baseUrl,
  requestHandle: (config) => {
    const header = {
      authorization: uni.getStorageSync('token'),
    };

    if (whiteList.includes(config.originUrl)) {
      delete header.authorization;
    }

    return {
      ...config,
      header,
    };
  },
  errorHandle: (error) => {
    console.log(error);
  },
});

Vue.prototype.$request = request;
export default request;
