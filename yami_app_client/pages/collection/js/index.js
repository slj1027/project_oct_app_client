import { getCollection } from '@/service/collection';

export default {
  name: 'collection',
  components: {},
  props: {},
  data() {
    return {
      page: { current: 1, size: 10 },
      list: [],
      total: 0,hasUser: false
    };
  },
  computed: {},
  methods: {
    async getCollectionList() {
      let res = await getCollection(this.page);
      this.list = this.list.concat([...res.records]);
      this.total = res.total;
    },
    async toDetail(id) {
      console.log(id);
      uni.navigateTo({ url: `/pages/details/index?id=${id}` });
    },
  },
  watch: {},

  // 页面周期函数--监听页面加载
  onLoad() {
    this.getCollectionList();
    this.hasUser = uni.getStorageSync('userInfo') ? true : false;
  },
  // 页面周期函数--监听页面初次渲染完成
  onReady() {},
  // 页面周期函数--监听页面显示(not-nvue)
  onShow() {},
  // 页面周期函数--监听页面隐藏
  onHide() {},
  // 页面周期函数--监听页面卸载
  onUnload() {},
  // 页面处理函数--监听用户下拉动作
  // onPullDownRefresh() { uni.stopPullDownRefresh(); },
  // 页面处理函数--监听用户上拉触底
  onReachBottom() {
    this.page.current++;
  },
  // 页面处理函数--监听页面滚动(not-nvue)
  // onPageScroll(event) {},
  // 页面处理函数--用户点击右上角分享
  // onShareAppMessage(options) {},
};
