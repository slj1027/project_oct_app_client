import {
	getOrderNumber,
	commitOrder,
	putOrder,
	getAllOrderDetail,
} from '@/service/order';
export default {
	name: 'page',
	props: {},
	data() {
		return {
			actualTotal: 0, //总价
			totalCount: 0, //总数
			coupons: [], //整个订单可以使用的优惠券列表
			orderReduce: 0, //订单优惠金额(所有店铺优惠金额相加)
			userAddr: null, //地址
			text: '',
			transfee: '', //运费,
			orderList: {}, //订单列表,
			orderNumber: '',
			orderListDetail: [],
			//优惠卷
			coupons: {
				totalLength: 0,
				canUseCoupons: [],
				unCanUseCoupons: [],
			},
			//选择的优惠卷
			couponIds: [],
			show: false,
		};
	},
	computed: {},
	methods: {
		showCoupons() {
			this.show = true;
		},
		checkCoupons(item) {
			if (this.couponIds.indexOf(item) === -1) {
				this.couponIds.push(item);
			} else {
				this.couponIds.splice(this.couponIds.indexOf(item), 1);
			}
		},
		sureCoupons() {
			if (this.couponIds.length > 0) {
				this.getOrderList({
					basketIds: this.basketIds.split(','),
					addrId: 0,
					couponIds: this.couponIds,
				});
			}
			this.show = false;
		},
		async submit() {
			if (this.orderNumber) {
				this.pay(this.orderNumber);
			} else {
				await getOrderNumber({
					orderShopParam: [{
						remarks: this.text,
						shopId: 1,
					}, ],
				}).then(
					(res) => {
						if (typeof res === 'string') {
							uni.showModal({
								title: '提示',
								content: res,
								success: ({
									confirm,
									cancel
								}) => {},
							});
						} else {
							this.pay(res.orderNumbers);
						}
					},
					(err) => {
						uni.showToast({
							title: err,
							icon: 'error',
							mask: true,
						});
					}
				);
			}
		},
		//支付
		async pay(orderNumber) {
			await putOrder({
				orderNumbers: orderNumber,
				payType: 1,
			}).then(
				(res) => {
					uni.requestPayment({
						...res,
						package: res.packageValue,
						success(res) {
							console.log(res, '11111111111111111111');
						},
						fail(res) {
							console.log(res, 'err');
						},
					});
				},
				(err) => {
					uni.showToast({
						title: err,
						icon: 'error',
						mask: true,
					});
				}
			);
		},
		//获取订单列表
		async getOrderList(obj) {
			let res = {};
			if (typeof obj == 'string') {
				res = await getAllOrderDetail({
					orderNumber: obj,
				});
				this.userAddr = res.userAddrDto
				this.transfee = res.transfee;
				this.orderListDetail = res.orderItemDtos;
			} else {
				res = await commitOrder(obj);
				this.userAddr = res.userAddr;
				res.shopCartOrders.forEach((a) => {
					this.transfee += a.transfee;
					!this.orderList[a.shopId] &&
						a.shopCartItemDiscounts.forEach((item) => {
							this.orderList[a.shopId] = [].concat(item.shopCartItems);
						});
				});
				if (res.shopCartOrders[0].coupons) {
					let canUseCoupons = [];
					let unCanUseCoupons = [];
					res.shopCartOrders[0].coupons.forEach((coupon) => {
						if (coupon.canUse) {
							canUseCoupons.push(coupon);
						} else {
							unCanUseCoupons.push(coupon);
						}
					});

					this.coupons = {
						totalLength: res.shopCartOrders[0].coupons.length,
						canUseCoupons: canUseCoupons,
						unCanUseCoupons: unCanUseCoupons,
					};
				}
			}

			this.actualTotal = res.actualTotal;
			this.totalCount = res.totalCount;
			this.orderReduce = res.orderReduce;

		},
	},
	watch: {},

	onLoad(options) {
		if (options.basketIds) {
			this.basketIds = options.basketIds;
			this.$store.commit('getOrderBasketIds', options.basketIds);
			this.getOrderList({
				basketIds: options.basketIds.split(','),
				addrId: 0,
			});
		} else if (options.orderNumber) {
			this.getOrderList(options.orderNumber);
		} else if (options.isCheck) {
			this.getOrderList({
				basketIds: this.$store.state.orderBasketIds.split(','),
				addrId: this.$store.state.addrId,
				couponIds: this.couponIds,
			});
		} else {
			this.getOrderList({
				orderItem: {
					...options
				},
				addrId: 0,
			});
		}
		this.orderNumber = options.orderNumber;
		
	},
};
