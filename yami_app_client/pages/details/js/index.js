import {
	getDetail,
	getDetailComment,
	getSku
} from '@/service/detail';
import {
	addCart,
	getCartLength
} from '@/service/cart';
import {
	addOrCancel,
	isInOrCancel
} from '@/service/collection';
import Popup from '@/components/popup/index';
export default {
	components: {
		Popup,
	},
	data() {
		return {
			show: false,
			prodId: '',
			skuId: '',
			show: false,
			detail: {},
			imgList: [],
			comment: {},
			isInCollect: false,
			options: [{
					icon: 'headphones',
					text: '客服',
				},
				{
					icon: 'cart',
					text: '购物车',
					info: 2,
				},
			],
			sku: [],
			buttonGroup: [{
					text: '加入购物车',
					backgroundColor: '#ff0000',
					color: '#fff',
				},
				{
					text: '立即购买',
					backgroundColor: '#ffa200',
					color: '#fff',
				},
			],
			showObj: {
				defaultSkuList: [], //sku展示选中项，
				properties: [],
				price: 0,
				pic: '',
			},
			num: 1,
		};
	},

	onLoad(option) {
		this.getDetailData(option.id);
		this.isInCollection(option.id);
		this.prodId = option.id;
	},

	methods: {
		//获取详情页数据
		async getDetailData(id) {
			let data = await getDetail(id);
			this.detail = data;
			this.detail.content = data.content.replace(
				/width\=\"\d+\"/g,
				'width="100%"'
			);
			console.log(/width\=\"\d+\"/.test(data.content));
			this.imgList = data.imgs.split(',');
			this.comment = await getDetailComment(id);
			this.showObj.defaultSkuList = data.skuList[0].skuName.split(' ');
			let result = await getSku(this.prodId);
			this.sku = result;
			this.skuId = this.sku[0].skuId;
		},
		//是否在收藏列表中
		async isInCollection(id) {
			let res = await isInOrCancel(id);
			this.isInCollect = res;
		},
		//点击商品导航按钮
		async buttonClick() {
			//加入购物车
			this.$refs.popup.open('bottom');
			this.formatSku(this.sku);
			this.showObj.price = this.detail.price;
			this.showObj.pic = this.detail.pic;
		},
		//判断有没有该商品
		hasSkuItem(selectedProperties) {
			let flag = this.detail.skuList.find(
				({
					skuName
				}) => selectedProperties.join(' ') === skuName
			);

			return flag;
		},
		//选中状态
		isActive({
			item,
			key
		}) {
			const selectedProperties = [...this.showObj.defaultSkuList];
			selectedProperties[key] = item;
			let className = '';
			this.showObj.defaultSkuList.forEach((v, i) => {
				if (key === i && item === v) {
					className = 'active';
				}
			});
			let flag = this.hasSkuItem(selectedProperties);
			className = flag ? className : className + 'disabled';
			return className;
		},
		formatSku(arr) {
			if (arr.length > 0) {
				const {
					properties,
					skuName
				} = arr[0];
				this.showObj.defaultSkuList = skuName.split(/\s/);
				if (properties) {
					this.showObj.properties = properties.split(';').map((item, index) => {
						const [title] = item.split(':');
						const titlePlus = [
							...new Set(arr.map(({
								skuName
							}) => skuName.split(' ')[index])),
						];
						return {
							title,
							titlePlus,
						};
					});
				}
			}
		},
		//改变商品规格选择
		changeChoose({
			item,
			key
		}) {
			let {
				defaultSkuList
			} = this.showObj;
			defaultSkuList[key] = item;
			this.showObj.defaultSkuList = [...defaultSkuList];
			this.sku.forEach((item) => {
				if (item.skuName === this.showObj.defaultSkuList.join(' ')) {
					this.skuId = item.skuId;
					this.showObj.pic = item.pic;
					this.showObj.price = item.price;
				}
			});
		},
		//修改购买个数
		onChange(event) {
			this.num = event.detail;
		},
		//加入购物车
		async addCart() {
			if (this.$store.state.user.nickName) {
				addCart({
					count: this.num,
					prodId: this.prodId,
					shopId: this.detail.shopId,
					skuId: this.skuId,
				}).then(async (res) => {
					if (res === '库存不足') {
						uni.showToast({
							title: res,
							icon: 'error',
							mask: true,
						});
					} else {
						this.$refs.popup.close();
						this.$store.dispatch('getCartLength')
					}
				});
			} else {
				uni.showToast({
					title: '你还没有登录/授权，不能购买此商品',
					icon: 'warning',
					mask: true,
				});
			}
		},
		//立即购买
		buy() {
			if (this.$store.state.user.nickName) {
				uni.navigateTo({
					url: `/pages/pay/index?prodCount=${[this.num]}&&prodId=${
            this.prodId
          }&&shopId=${this.detail.shopId}&&skuId=${this.skuId}`,
				});
			} else {
				uni.showToast({
					title: '你还没有登录/授权，不能购买此商品',
					icon: 'warning',
					mask: true,
				});
			}
		},
		//收藏
		async collection() {
			await addOrCancel(this.prodId);
			await this.isInCollection(this.prodId);
		},
		//评论
		toComment() {
			this.show = true;
		},
		close() {
			this.show = false;
		},
	},
};
