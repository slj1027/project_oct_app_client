import {
	getCart,
	addCart,
	getCartLength,
	delCart,
	getLapseLength
} from '@/service/cart';
export default {
	data() {
		return {
			cartList: [],
			checked: false,
			basketIds: [],
			hasUser: false,
			lapseList: []
		};
	},
	onShow() {
		this.getCart();
		this.getLapse();
		this.hasUser = uni.getStorageSync('userInfo') ? true : false;
	},
	computed: {
		//总价
		price() {
			let price = 0;
			this.cartList.forEach((item) => {
				if (this.basketIds.includes(item.basketId.toString())) {
					price += item.prodCount * item.price;
				}
			});
			return price * 100;
		},
	},
	methods: {
		//获取购物车数据
		async getCart() {
			let res = await getCart({});
			this.cartList = [];
			res[0].shopCartItemDiscounts[0].shopCartItems.forEach((a) => {
				this.cartList.push(a);
			});
		},
		//获取失效商品数据
		async getLapse() {
			let res = await getLapseLength();
			console.log(res)
			this.lapseList = res
			/* this.cartList = [];
			res[0].shopCartItemDiscounts[0].shopCartItems.forEach((a) => {
				this.cartList.push(a);
			}); */
		},
		//购物车数量++
		async addProdCount(e) {
			this.cartList.forEach((item) => {
				if (item.basketId === e.target.dataset.id) {
					let obj = {
						basketId: item.basketId,
						prodId: item.prodId,
						shopId: item.shopId,
						skuId: item.skuId,
						count: 1,
					};
					this.editCart(obj)
				}
			});
		},
		//购物车数量--
		async subProdCount(e) {
			this.cartList.forEach((item) => {
				if (item.basketId === e.target.dataset.id) {
					let obj = {
						basketId: item.basketId,
						prodId: item.prodId,
						shopId: item.shopId,
						skuId: item.skuId,
						count: -1,
					};
					this.editCart(obj);
				}
			});
		},
		//购物车数量输入
		async changeProCount(e) {
			this.cartList.forEach((item) => {
				if (item.basketId === e.target.dataset.id) {
					let obj = {
						basketId: item.basketId,
						prodId: item.prodId,
						shopId: item.shopId,
						skuId: item.skuId,
						count: e.detail.value - item.prodCount,
					};
					this.editCart(obj);
				}
			});
		},
		editCart(obj) {
			addCart(obj).then(async (res) => {
				await this.getCart();
				if (res.statusCode === 200) {
					await this.cartLength();
				} else {
					if (res === '库存不足') {
						uni.showToast({
							title: res,
							icon: 'error',
						})
					}
				}

			});
		},
		//购物车长度
		async cartLength() {
			this.$store.dispatch('getCartLength')
		},

		//正选
		checkboxChange(e) {
			this.basketIds = e.detail.value;
			if (this.basketIds.length === this.cartList.length) {
				this.checked = true;
			} else {
				this.checked = false;
			}
		},
		//全选
		changeAllCheck(e) {
			if (e.detail.value[0] === 'all') {
				this.checked = true;
				let arr = [];
				this.cartList.forEach((item) => {
					arr.push(item.basketId.toString());
				});
				this.basketIds = arr;
			} else {
				this.checked = false;
				this.basketIds = [];
			}
		},
		//提交订单
		async submit() {
			if (this.basketIds.length > 0) {
				uni.navigateTo({
					url: `/pages/pay/index?basketIds=${this.basketIds}`,
				});
			} else {
				uni.showToast({
					title: '你没有选择购买的商品',
					icon: 'error',
				});
			}
		},
		//删除购物车选中商品
		async del() {
			if (this.basketIds.length === 0) {
				uni.showToast({
					title: '请选择商品',
					icon: 'error',
				});
			} else {
				uni.showModal({
					title: '',
					content: '确认要删除选中的商品吗?',
					confirmColor: '#eb2444',
					success: async (res) => {
						if (res.confirm) {
							await delCart(this.basketIds);
							await this.getCart();
							await this.cartLength();
						} else if (res.cancel) {}
					},
				});
			}
		},
	},
};
