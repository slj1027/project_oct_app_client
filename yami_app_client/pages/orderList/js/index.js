import {
	getAllOrderList,
	cancelOrder,
	putOrder,
	delOrder,
	hadOrder,
	comment,
} from '@/service/order';
export default {
	data() {
		return {
			comment: {
				content: '',
				score: 5,
				evaluate: 0,
				isAnonymous: false,
				orderItemId: '',
				pics: '',
				prodId: '',
			},
			show: false,
			active: 0,
			list: [],
			current: 1,
			size: 5,
			total: 0,
			hasUser: false,
			value: 5,
			time: null
		};
	},
	onLoad(options) {
		this.active = Number(options.id);
		this.getOrderList({
			page: 1,
			size: 5,
			status: this.active,
		});
		this.hasUser = uni.getStorageSync('userInfo') ? true : false;
		this.time = setInterval(() => {
			this.list.forEach((item) => {
				console.log(item)
			})
		}, 1000)
	},
	methods: {
		showPopup(orderNumber) {
			this.show = true;
			/* this.comment.orderItemId = orderNumber; */
		},

		onClose() {
			this.show = false;
		},
		//获取订单列表
		async getOrderList(obj) {
			let res = await getAllOrderList(obj);
			res.records.forEach((item) => {
				item.count = 0;
				item.orderItemDtos.forEach((a) => {
					item.count += a.prodCount;
				});
			});
			this.total = res.total;

			if (obj.page === 1) {
				this.list = [...res.records];
			} else {
				this.list = this.list.concat([...res.records]);
			}
		},
		onChange(e) {
			this.active = e.detail.index;
			this.getOrderList({
				page: 1,
				size: 5,
				status: e.detail.index,
			});
		},
		//取消订单
		cancel(orderNumber) {
			cancelOrder(orderNumber).then((res) => {
				this.getOrderList({
					page: 1,
					size: 5,
					status: this.active,
				});
			});
		},
		//重新购买
		againToBuy(orderNumber) {
			uni.navigateTo({
				url: '/pages/pay/index?orderNumber=' + orderNumber
			});
		},
		//付款
		async pay(orderNumber) {
			await putOrder({
				orderNumbers: orderNumber,
				payType: 1,
			}).then((res) => {
				uni.showModal({
					title: '提示',
					content: JSON.stringify(res),
					showCancel: true,
					success: ({
						confirm,
						cancel
					}) => {
						if (confirm) {}
					},
				});
			});
		},
		async del(orderNumber) {
			uni.showModal({
				title: '提示',
				content: '确认要删除订单吗？',
				showCancel: true,
				success: ({
					confirm,
					cancel
				}) => {
					if (confirm) {
						delOrder(orderNumber).then((res) => {
							this.getOrderList({
								page: 1,
								size: 5,
								status: this.active,
							});
						});
					}
				},
			});
		},
		//确认收货
		async sureGetOrder(orderNumber) {
			uni.showModal({
				title: '提示',
				content: '确认已经收货了吗？',
				showCancel: true,
				success: ({
					confirm,
					cancel
				}) => {
					if (confirm) {
						hadOrder(orderNumber).then((res) => {
							this.getOrderList({
								page: 1,
								size: this.size,
								status: this.active,
							});
						});
					}
				},
			});
		},

		onRateChange(e) {
			console.log(e);
			this.comment.score = e.detail;
		},
		async addComment() {
			let res = await comment(this.comment);
			console.log(res);
		},
	},
	// 页面处理函数--监听用户上拉触底
	onReachBottom() {
		if (this.total > this.list.length) {
			this.current++;
			this.getOrderList({
				page: this.current,
				size: 5,
				status: this.active,
			});
		}
	},
};
